import mlflow
import os
from tqdm import tqdm


def main():
    gen_files("oa")
    gen_files("ya")


def gen_files(participant_type):
    ml_client = mlflow.tracking.MlflowClient("../.mlruns")
    os.makedirs(f"outputs/{participant_type}", exist_ok=True)
    runs = ml_client.search_runs("2", f"params.participant_type='{participant_type}' and params.data_type='all'")
    print(f"Got {len(runs)} entries")
    print(f"Generating files for {participant_type}: ")
    with open(f"outputs/{participant_type}/conf_matrix_test_single_file.txt", "w") as all_f:
        for run in tqdm(runs):
            name = run.data.params['name']
            if 'all' in name:
                continue
            run_uuid = run.info.run_id
            all_f.write(f"{participant_type}_{name.split('_')[-1]}\n")
            with open(ml_client.download_artifacts(run_uuid, 'conf_matrix_test.txt')) as f:
                lines = f.readlines()
                all_f.writelines(lines)
                with open(f"outputs/{participant_type}/{name}_conf_matrix_test.txt", "w") as new_f:
                    new_f.writelines(lines)
                all_f.write("\n\n")


if __name__ == '__main__':
    main()
