from __future__ import print_function

import sys
import pygame
from pygame.locals import *
import myo
import common
import numpy as np

if __name__ == '__main__':
    classifiers = []
    handlers = []

    pygame.init()
    font = pygame.font.Font(None, 30)
    myo_handler = myo.Myo(tty=sys.argv[1] if len(sys.argv) >= 2 else None, emg=True, imu=False)

    w, h = 800, len(myo_handler.classifiers) * 320 + 100
    scr = pygame.display.set_mode((w, h))

    for _, classifier in myo_handler.classifiers.items():
        classifiers.append(classifier)

    for _, handler in myo_handler.handlers.items():
        handlers.append(handler)

    myo_handler.connect()

    myo_handler.sleep_mode(1)
    myo_handler.set_leds([128, 128, 255], [128, 128, 255])  # purple logo and bar LEDs
    myo_handler.vibrate(1)

    SCORES = np.zeros([len(myo_handler.classifiers), 10])


    try:
        while True:
            myo_handler.run()

            for ev in pygame.event.get():
                if ev.type == QUIT or (ev.type == KEYDOWN and ev.unicode == 'q'):
                    raise KeyboardInterrupt()
                elif ev.type == KEYDOWN:
                    if K_0 <= ev.key <= K_9:

                        for handler in handlers:
                            handler.recording = ev.key - K_0

                    elif K_KP0 <= ev.key <= K_KP9:

                        for handler in handlers:
                            handler.recording = ev.key - K_KP0

                    elif ev.unicode == 'r':

                        for classifier in classifiers:
                            classifier.read_data()

                elif ev.type == KEYUP:
                    if K_0 <= ev.key <= K_9 or K_KP0 <= ev.key <= K_KP9:

                        for handler in handlers:
                            handler.recording = -1

            y_offset = 0
            scr.fill((0, 0, 0), (0, 0, w, h))
            for class_index, (handler, classifier) in enumerate(zip(handlers, classifiers)):
                history = myo_handler.histories[classifier.name]
                r = history.history_cnt.most_common(1)[0][0]

                for i in range(10):
                    x = 0
                    y = y_offset + 0 + 30 * i

                    clr = (0, 200, 0) if i == r else (255,255,255)

                    txt = font.render('%5d' % (classifier.Y == i).sum(), True, (255, 255, 255))
                    scr.blit(txt, (x + 20, y))

                    txt = font.render('%d' % i, True, clr)
                    scr.blit(txt, (x + 110, y))

                    scr.fill((0, 0, 0), (x+130, y + txt.get_height() / 2 - 10, len(history.history) * 20, 20))
                    scr.fill(clr, (x+130, y + txt.get_height() / 2 - 10, history.history_cnt[i] * 20, 20))
                    SCORES[class_index, i] = history.history_cnt[i] / len(history.history)

                if classifier.nn is not None:
                    reshaped_data = handler.data.reshape(1, classifier.columns)
                    dists, inds = classifier.nn.kneighbors(reshaped_data)
                    for i, (d, ind) in enumerate(zip(dists[0], inds[0])):
                        y = classifier.Y[myo.SUBSAMPLE * ind]
                        fmt ='%d %'+str(classifier.columns)+'d'
                        common.text(scr, font, fmt % (y, d), (650, (20 * i) + y_offset))

                y_offset += 350
                for k in range(len(SCORES)):
                    for l in range(len(SCORES[k])):
                        if SCORES[k][l] > 0.9:
                            if 0 < l < 9:
                                scr.blit(font.render('KNN RESULT: class \"{0:s}\", gesture #{1:d}'
                                                 .format(classifier.name, l),True, (0, 200, 0)), (20 + 20, y_offset))
                                break



            pygame.display.flip()

    except KeyboardInterrupt:
        pass
    finally:
        print("Sleep Mode is turned back ON. Disconnecting...")
        myo_handler.sleep_mode(0)
        myo_handler.set_leds([0, 0, 0], [0, 0, 0])  # purple logo and bar LEDs
        myo_handler.mc_end_collection()
        myo_handler.disconnect()

    pygame.quit()
