import os
import logging
import shutil
import json
# from pywt import cwt
# import neurokit as nk
from random import choices
import random
from mlpipeline import log
from mlpipeline.base import DataLoaderABC
import seaborn as sn
import pandas as pd
import mlflow
import numpy as np
import matplotlib.pyplot as plt

# Setting the seeds for different modules
random.seed(1)
np.random.seed(1)


def create_models_dir_struct(export_dir):
    try:
        # Create target Directory
        os.mkdir(export_dir)
        print("Directory " + str(export_dir) + " has been created ")
    except FileExistsError:
        pass


def _grab_center_data_points(df, window_size, dimensions):
    start = int((df.shape[0] - window_size)/2)
    d = df[start:start+window_size, 0:dimensions]
    return d


# compute fft features (amplitude of frequencies)
def compute_imu_features(df, parameters):
    if 'data' not in parameters.name:
        df = df[:, 1:]
    df = df[:, :parameters.imu_dimensions]
    d = _grab_center_data_points(df,
                                 parameters.imu_window_size,
                                 parameters.imu_dimensions)
    fft = np.abs(np.fft.fft(d, axis=0))
    fft = fft.reshape(-1)
    assert fft.shape[0] == parameters.imu_dimensions * parameters.imu_window_size, \
        "{}".format(fft.shape)
    return fft


def compute_emg_features(df, parameters):
    if parameters.name != 'data3':
        df = df[:, 1:]
    df = abs(df)
    d = _grab_center_data_points(df,
                                 parameters.emg_window_size,
                                 parameters.emg_dimensions)
    # #------Approach 1 (Using CWT)-----
    # result = np.concatenate(
    #     [cwt(d[:, i], 1, 'mexh', method='fft')[0].reshape(-1, 1) for i in range(d.shape[1])],
    #     axis=1).reshape(-1)
    # #------Approach 2 (nk's emg_process)-----
    # plt.figure(1)
    # plt.plot(df)
    # plt.show()
    # df = np.abs(df)
    # df = nk.emg_process(d)['df']
    # df = df.loc[:, [f"EMG_{i}_Filtered" for i in range(get_data_parameters().emg_dimensions)]]
    # df = df.to_numpy()

    # #------Approach 3 (Using raw values from the data)-----
    result = np.concatenate(
        [d[:, i].reshape(-1, 1) for i in range(d.shape[1])],
        axis=1).reshape(-1)
    assert result.shape[0] == parameters.emg_dimensions * parameters.emg_window_size, \
        "{}".format(result.shape)
    return result


# Using list as the parameter to maintain similar function signature for all the compute_*_features functions
def compute_all_features(df, parameters):
    '''
    df should be an sequence object with the first being the emg features and the second being the imu featurs
    '''
    emg_df, imu_df = df
    emg_df = compute_emg_features(emg_df, parameters)
    imu_df = compute_imu_features(imu_df, parameters)
    result = np.concatenate((emg_df, imu_df), axis=0)
    input_size = parameters.emg_dimensions * parameters.emg_window_size + \
        parameters.imu_dimensions * parameters.imu_window_size
    assert result.shape[0] == input_size
    return result


def build_data(activities, n_components, _pca=None, start=None, end=None):
    training_x = np.empty(shape=(0, n_components if _pca is not None
                                 else activities[0][0][start:end].shape[0]))
    training_y = np.empty(shape=(0, 1))
    for (_x, _y) in activities:
        _x = np.expand_dims(_x[start:end], axis=0)
        reduced_x = _pca.transform(_x) if _pca is not None else _x
        training_x = np.append(training_x, reduced_x, axis=0)
        training_y = np.append(training_y, _y)
    return training_x, training_y


def _load_file_from_path(file_path, cls, data_dir, file_index, is_training_data):
    path = file_path / cls / data_dir / (str(file_index) + '.txt')
    try:
        raw_x = np.loadtxt(path, dtype=np.float32, delimiter=',')
        if raw_x.size == 0:
            return None
        return raw_x
    except (FileNotFoundError, OSError):
        return None


def load_data(parameters, data_type='imu', current_class=None):
    train_data = []
    test_data = []
    temp_data = []
    encoding = {}
    classes = parameters.classes
    file_path = parameters.data_path
    if data_type == 'imu':
        data_dir = 'imu'
    elif data_type == 'emg':
        data_dir = 'emg'  # In the event that both are to be used.
    elif data_type == 'all':
        data_dir = 'all'
    else:
        raise ValueError("For `data_type` expecting one of `imu`, `emg` or `all`. But got {}".format())
    break_counter = 0
    break_condition = 15
    for idx, cls in enumerate(classes):
        file_index = 1
        n_total = parameters.n_total
        n_training = parameters.n_training
        while file_index <= n_total:
            is_training_data = file_index <= n_training
            if data_dir in ['imu', 'emg']:
                raw_x = _load_file_from_path(file_path, cls, data_dir, file_index, is_training_data)
                if raw_x is None:
                    log(f"empty file encountered: `{file_path}`, index `{file_index}` for `{data_dir}` data with class `{cls}`")
                    n_total += 1
                    file_index += 1
                    if is_training_data:
                        n_training += 1
                    if break_counter > break_condition:
                        log(f"Skip error: with  `{file_path}` in class `{cls}` "
                            f" type `all`  file `{file_index}`",
                            logging.ERROR)
                        raise Exception("Skipped too many files.")
                    break_counter += 1
                    continue
                if data_type == 'imu':
                    try:
                        raw_x = compute_imu_features(raw_x, parameters)
                    except AssertionError:
                        log(f"AssertionError: with  `{file_path}` in class `{cls}` "
                            f" type `{data_dir}`  file `{file_index}`, shape {raw_x.shape}",
                            logging.WARNING)
                        n_total += 1
                        file_index += 1
                        if is_training_data:
                            n_training += 1
                        if break_counter > break_condition:
                            raise Exception("Skipped too many files.")
                        break_counter += 1
                        continue
                if data_type == 'emg':
                    try:
                        raw_x = compute_emg_features(raw_x, parameters)
                    except AssertionError:
                        log(f"AssertionError: with  `{file_path}` in class `{cls}` "
                            f" type `{data_dir}`  file `{file_index}`, shape {raw_x.shape}",
                            logging.WARNING)
                        n_total += 1
                        file_index += 1
                        if is_training_data:
                            n_training += 1
                        if break_counter > break_condition:
                            raise Exception("Skipped too many files.")
                        break_counter += 1
                        continue
            else:
                raw_x_emg = _load_file_from_path(file_path, cls, 'emg', file_index, is_training_data)
                raw_x_imu = _load_file_from_path(file_path, cls, 'imu', file_index, is_training_data)
                if raw_x_emg is None or raw_x_imu is None:
                    if raw_x_emg is None:
                        log(f"empty file encountered: `{file_path}`, index `{file_index}` for `emg` data with class `{cls}`")
                    if raw_x_imu is None:
                        log(f"empty file encountered: `{file_path}`, index `{file_index}` for `imu` data with class `{cls}`")
                    # if file_index > 1:
                    n_total += 1
                    file_index += 1
                    if is_training_data:
                        n_training += 1
                    if break_counter > break_condition:
                        log(f"Skip error: with  `{file_path}` in class `{cls}` "
                            f" type `all`  file `{file_index}`",
                            logging.ERROR)
                        raise Exception("Skipped too many files.")
                    break_counter += 1
                    continue
                    # else:
                    #     break
                    # break
                try:
                    raw_x = compute_all_features([raw_x_emg, raw_x_imu], parameters)
                except AssertionError:
                    log(f"AssertionError: with  `{file_path}` in class `{cls}` "
                        f" type `all`  file `{file_index}`, shape {raw_x_emg.shape}, {raw_x_imu.shape}",
                        logging.WARNING)
                    n_total += 1
                    file_index += 1
                    if is_training_data:
                        n_training += 1
                    if break_counter > break_condition:
                        raise Exception("Skipped too many files.")
                    break_counter += 1
                    continue
            _x = raw_x
            if is_training_data:
                if current_class is None:
                    train_data.append((_x, idx))
                    encoding[idx] = cls
                elif cls == current_class:
                    train_data.append((_x, 1))
                    encoding[1] = cls
                else:
                    temp_data.append((_x, 0))
                    encoding[0] = "other"
            else:
                if current_class is None:
                    test_data.append((_x, idx))
                    encoding[idx] = cls
                elif cls == current_class:
                    test_data.append((_x, 1))
                    encoding[1] = cls
                else:
                    test_data.append((_x, 0))
                    encoding[0] = "other"
            file_index += 1

    if len(temp_data) > 0:
        train_data.extend(choices(temp_data, k=len(train_data)))
    log('training activities: {}'.format(len(train_data)), agent='Data loading')
    log('testing activities: {}'.format(len(test_data)), agent='Data loading')
    log('encoding: {}'.format(encoding), agent='Data loading')
    return train_data, test_data, encoding


def log_conf_metrix(conf_matrix, suffix, experiment_dir, encoding):
    if suffix == 0:
        suffix = "train"
    elif suffix == 1:
        suffix = "test"
    log("Confusion matrix: \n{}".format(str(conf_matrix)), log_to_file=True)
    os.makedirs("../.tmp", exist_ok=True)
    np.savetxt(f"../.tmp/conf_matrix_{suffix}.txt", conf_matrix)
    conf_matrix = pd.DataFrame(conf_matrix)
    encoding = {int(k): v.replace('volume', 'vol').replace('video', 'vid') for k, v in encoding.items()}
    conf_matrix = conf_matrix.rename(columns=encoding, index=encoding)
    plt.figure(figsize=(12, 10))
    sn.set(font_scale=0.9)
    ax = sn.heatmap(conf_matrix, annot=True, square=True, linewidths=0.3)
    plt.ylabel("Ground Truth")
    plt.xlabel("Predicted")
    ax.set_ylim(len(encoding), 0)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=30)
    # plt.show()
    plt.savefig(f'../.tmp/conf_matrix_fig_{suffix}.png')
    mlflow.log_artifact(f'../.tmp/conf_matrix_fig_{suffix}.png')
    shutil.copy(f"../.tmp/conf_matrix_{suffix}.txt", experiment_dir)
    mlflow.log_artifact(f"../.tmp/conf_matrix_{suffix}.txt")
    plt.close()

    ground_truth = conf_matrix.sum(axis=1)
    predicted = conf_matrix.sum(axis=0)
    recall = {k: conf_matrix.loc[k, k]/ground_truth[k] for k in encoding.values()}
    precision = {k: conf_matrix.loc[k, k]/predicted[k] for k in encoding.values()}
    f1 = {k: (2 * precision[k] * recall[k])/(precision[k] + recall[k]) for k in encoding.values()}
    plt.figure(figsize=(8, 8))
    ax = sn.barplot(list(f1.keys()), list(f1.values()))
    sn.set(font_scale=1)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=30)
    ax.set(ylabel='F1 score')
    plt.savefig(f'../.tmp/bar_plot_fig_{suffix}.png')
    mlflow.log_artifact(f'../.tmp/bar_plot_fig_{suffix}.png')
    mlflow.log_param(f"Low classes -less than 0.5 - {suffix}", [k for k, v in f1.items() if v < 0.5])
    plt.close()

    with open(f'../.tmp/encoding_{suffix}.json', 'w') as f:
        json.dump(encoding, f)
    mlflow.log_artifact(f'../.tmp/encoding_{suffix}.json')
    shutil.copy(f'../.tmp/encoding_{suffix}.json', experiment_dir)


class DataLoader(DataLoaderABC):
    def __init__(self, parameters, data_type='imu', current_class=None):
        self.train_data, self.test_data, self.encoding = load_data(parameters, data_type, current_class)
        # training_X = pca.transform(all_training_x)
        # testing_X = pca.transform(all_testing_x)
        # TODO: Is this correct?

    def get_test_input(self, **kwargs):
        return self.test_data

    def get_test_sample_count(self, **kwargs):
        return len(self.test_data[0])

    def get_train_input(self, **kwargs):
        return self.train_data

    def get_train_sample_count(self, **kwargs):
        return len(self.train_data[0])
